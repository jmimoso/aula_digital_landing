const commonPaths = require('./paths');

module.exports = {
  mode: 'development',
  devServer: {
    host: '192.168.2.22',
    port: 12345
  },
  output: {
    filename: '[name].js',
    path: commonPaths.outputPath,
    chunkFilename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        loader: 'css-loader',
          options: {
            sourceMap: true,
            modules: true,
            localIdentName: '[local]___[hash:base64:5]',
          }
      }, {
          test: /\.scss$/,
          use: [
              "style-loader",
              "css-loader",
              "sass-loader"
          ]
      }
    ],
  },
  devtool: 'source-map'
};