import React from 'react';
import ReactDOM from 'react-dom';

import 'whatwg-fetch';
import 'es6-promise/auto';

import App from './js/App';

import styles from './styles/main.scss';


ReactDOM.render(<App />, document.getElementById('root'));