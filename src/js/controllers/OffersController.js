import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';


class OffersController extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentWillMount() {
	}

	render() {
		return (
			<section className='offers-wrapper'>
				<section className='offers-container'>
					<div className="top-wrapper">
						<div className="top-container">
							<h2 className="title">
								Aula digital <span>Oferta</span>
							</h2>
							<h5 className="description">
								Ative a licença OFERTA ESCOLAR e código de acesso aos produtos AULA DIGITAL.
							</h5>
							<div className="button-group">
								<div className="button-wrapper">
									<Link className='button' to='section_1' smooth={true} duration={500}>
										O que é?
									</Link>
								</div>
								<div className="button-wrapper">
									<Link className='button' to='section_2' smooth={true} duration={500}>
										O que inclui?
									</Link>
								</div>
								<div className="button-wrapper">
									<Link className='button' to='section_3' smooth={true} duration={500}>
										Como ativar?
									</Link>
								</div>
							</div>
						</div>
					</div>
					<div className="bottom-wrapper">
						<div className="bottom-container">
							<Element className='section-wrapper' name='section_1'>
								111111111
								
							</Element>
							<Element className='section-wrapper' name='section_2'>
								2222222222
							</Element>
							<Element className='section-wrapper' name='section_3'>
								333333333333
							</Element>
						</div>
					</div>
				</section>
			</section>
		);
	}


}

export default OffersController;