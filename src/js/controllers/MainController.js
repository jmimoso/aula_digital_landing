import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Favicon from 'react-favicon';

import EntryController from './EntryController';
import OffersController from './OffersController';
import ActivationsController from './ActivationsController';

import HeaderComponent from './../components/HeaderComponent';
import FooterComponent from './../components/FooterComponent';


class MainController extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentWillMount() {
	}

	render() {
		return (
			<section className='main-wrapper'>
				<Favicon url={require('../../assets/images/favicon.ico')} />
				<section className='main-container'>
					<HeaderComponent />
					<Switch>
						<Route exact path='/' render={() => <EntryController />} />
						<Route exact path='/offers' render={() => <OffersController />} />
						<Route exact path='/activations' render={() => <ActivationsController />} />
					</Switch>
					<FooterComponent />
				</section>
			</section>
		);
	}


}

export default MainController;