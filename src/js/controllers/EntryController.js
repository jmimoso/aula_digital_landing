import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class EntryController extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentWillMount() {
	}

	render() {
		return (
			<section className='entry-wrapper'>
				<section className='entry-container'>
					<div className="highlight-wrapper">
						<div className="highlight-container">
							<div className="hightlight-pane">
								<h1 className="title">
									Aprender é incrível.
								</h1>
								<h2 className="subtitle">
									Aula digital <span>Oferta</span>
								</h2>
								<h5 className="description">
									Acesso gratuito aos conteúdos digitais dos manuais escolares dos 1º e ao 6º ano.
								</h5>
								<div className="button-wrapper">
									<Link className='button' to='/offers'>
										Saber mais
									</Link>
								</div>
								<div className="mega-image"></div>
							</div>
						</div>
					</div>
					<div className="ambit-wrapper">
						<div className="ambit-container">
							<div className="ambit activation">
								<h3 className="title">
									Ativar código
								</h3>
								<h5 className="description">
									Prentede ativar um código para ter acesso a um produto digital?
								</h5>
								<div className="button-wrapper">
									<Link className='button secondary' to='/activations'>
										Saber mais
									</Link>
									<a className='button' href="#">
										Ativar
									</a>
								</div>
							</div>
							<div className="ambit support">
								<h3 className="title">
									Suporte
								</h3>
								<h5 className="description">
									Tem dúvidas ou precisa de ajuda na utilização dos produtos Aula Digital?<br/>Consulte a área de suporte.
								</h5>
								<div className="button-wrapper">
									<a className='button' href="#">
										Entrar
									</a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		);
	}


}

export default EntryController;