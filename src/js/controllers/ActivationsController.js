import React, { Component } from 'react';


class ActivationsController extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentWillMount() {
	}

	render() {
		return (
			<section className='activations-wrapper'>
				<section className='activations-container'>
					<div className="top-wrapper">
						<div className="top-container">
							<h2 className="title">
								Ativar licenças
							</h2>
							<h5 className="description">
								Ative a licença OFERTA ESCOLAR e código de acesso aos produtos AULA DIGITAL.
							</h5>
						</div>
					</div>
					<div className="bottom-wrapper">
						<div className="bottom-container">

						</div>
					</div>
				</section>
			</section>
		);
	}


}

export default ActivationsController;