import React, { Component } from 'react';
import { HashRouter as Router } from 'react-router-dom';

import MainController from './controllers/MainController';


export default class App extends Component {

	render() {
		return (
			<Router>
				<MainController />
			</Router>
		);
	}

}