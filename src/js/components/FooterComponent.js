import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class FooterComponent extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentWillMount() {
	}

	render() {
		return (
			<div className='footer-wrapper'>
				<div className='footer-container'>
					<div className="upper_footer-wrapper">
						<div className="upper_footer-container">
							<div className="info-block">
								<h4 className="info-title">
									Informações
								</h4>
								<ul className="info-group">
									<li className="info-item">
										<Link to='/offers' className="info-link">
											Aula Digital - Oferta
										</Link>
									</li>
									<li className="info-item">
										<Link to='/activations' className="info-link">
											Ativação
										</Link>
									</li>
									<li className="info-item">
										<a href="https://suporteauladigital.leya.com/" className="info-link">
											Suporte
										</a>
									</li>
								</ul>
							</div>
							<div className="partners-block">
								<ul className="partners-group">
									<li className="partners-item">
										<img className='partners-image' src={require('../../assets/images/asaLogo.svg')} alt="LeYa Educação"/>
									</li>
									<li className="partners-item">
										<img className='partners-image' src={require('../../assets/images/gailivroLogo.svg')} alt="LeYa Educação"/>
									</li>
									<li className="partners-item">
										<img className='partners-image' src={require('../../assets/images/textoLogo.svg')} alt="LeYa Educação"/>
									</li>
									<li className="partners-item">
										<img className='partners-image' src={require('../../assets/images/sebentaLogo.svg')} alt="LeYa Educação"/>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div className="lower_footer-wrapper">
						<div className="lower_footer-container">
							<div className="logo-block">
								<img className='logo-image' src={require('../../assets/images/leyaEdLogo.svg')} alt="LeYa Educação"/>
							</div>
							<ul className="links-block">
								<li className="link-wrapper">
									<a href="http://leya.com/" target='_blank' className="link">
										LeYa
									</a>
								</li>
								<li className="link-wrapper">
									<a href="https://www.leyaeducacao.com/" target='_blank' className="link">
										LeYa Educação
									</a>
								</li>
								<li className="link-wrapper">
									<a href="https://www.leyaeducacao.com/z_sobrenos/i_65/" target='_blank' className="link">
										Política de Privacidade
									</a>
								</li>
								<li className="link-wrapper">
									<a href="https://www.leyaeducacao.com/z_sobrenos/i_369/" target='_blank' className="link">
										Termos e Condições
									</a>
								</li>
								<li className="link-wrapper">
									<a href="https://www.leyaeducacao.com/z_sobrenos/i_66/" target='_blank' className="link">
										Contactos
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}


}

export default FooterComponent;