import React, { Component } from 'react';
import { withRouter, NavLink, Link } from 'react-router-dom';
import ReactSVG from 'react-svg';

class HeaderComponent extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentWillMount() {
	}

	render() {
		return (
			<div className='header-wrapper'>
				<div className='header-container'>
					<div className="logo-wrapper">
						<ReactSVG className='logo-container' src={require('../../assets/images/logo.svg')} onClick={() => this.props.history.push('/')} />
					</div>
					<div className="navigation-wrapper">
						<div className="navigation-container">
							<ul className="navigation-items">
								<li className="navigation-item">
									<NavLink className='button minimal' activeClassName='active' to='/offers'>
										Oferta
									</NavLink>
								</li>
								<li className="navigation-item">
									<NavLink className='button minimal' activeClassName='active' to='/activations'>
										Ativação
									</NavLink>
								</li>
								<li className="navigation-item">
									<a className='button minimal' href='#'>
										Registar
									</a>
								</li>
								<li className="navigation-item">
									<a className='button' href='https://20.leya.com/catalogs/'>
										Entrar
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}


}

export default withRouter(HeaderComponent);